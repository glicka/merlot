FROM python:3.11-bullseye
MAINTAINER xyz

RUN pip install pytest

ENTRYPOINT [ "pytest" ]