FIRSTARG := $(firstword $(MAKECMDGOALS))
# use the rest as arguments
RUNARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
# ...and turn them into do-nothing targets
$(eval $(RUNARGS):;@:)

build:
	if [ "$(FIRSTARG)" = "build" ]; then \
		pip install . ;\
	elif [ "$(FIRSTARG)" = "docker" ]; then \
		sudo docker build -f docker/Dockerfile --tag merlot . ;\
	fi;

clean:
	if [ "$(FIRSTARG)" = "clean" ]; then \
		pip uninstall merlot -y ;\
		rm -rf build/ ;\
		rm -rf dist ;\
		rm -rf *.egg-info ;\
		rm -rf GreatDeluge*/ ;\
		rm -rf */__pycache__ ;\
	elif [ "$(FIRSTARG)" = "docker" ]; then \
		sudo docker system prune -af ;\
	fi;
	
test:
	if [ "$(FIRSTARG)" = "test" ]; then \
		coverage run -m pytest ;\
	elif [ "$(FIRSTARG)" = "docker" ]; then \
		sudo docker run merlot:latest ;\
	fi; 

run:
	python merlot/runBrowser.py

extract-docker-data:
	container_id=$(docker create "$image")
	sudo docker cp "$(container_id):$(source_path)" "$(destination_path)"
	sudo docker rm "$container_id"

docker : $(RUNARGS)

.PHONY: docker build test prune