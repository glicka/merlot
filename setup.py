#!/usr/bin/env python

from distutils.core import setup

setup(name='merlot',
      version='0.0.2',
      description='PyQt6 Internet browser',
      author='Adam Glick',
      author_email='glicka@proton.me',
      url='https://gitlab.com/glicka/merlot',
      packages=['merlot', 'sql', 'backend', 'attributes'],
      package_dir={
        'merlot': 'merlot',
        'sql': 'merlot/sql',
        'backend': 'merlot/backend',
        'attributes': 'merlot/attributes'
      },
      license="GNU Lesser General Public License v3.0"
     )