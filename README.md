# Merlot

## Getting started

- Clone branch
```
git clone https://gitlab.com/glicka/merlot.git
```
- Set a tolken after cloning branch
```
cd merlot
git remote set-url origin https://<TolkenName>:<Tolken>@gitlab.com/glicka/merlot.git
```
- Create a feature branch off of main
```
git checkout -b <lastname><firstinitial>-<feature>-<name>-<separated>-<by>-<hyphens>
```
- Push changes
```
git push -u origin <feature>-<branch>-<name>
```

## Requirements
### QT6
```
conda install -c conda-forge libffi
```
### MySQL
```
sudo apt install mysql-server
```
### Python Modules
```
pip install -r requirements.txt
```

## Setup MySQl

- Enter MySQL shell and check if user merlot exists
```
sudo mysql
SELECT Host,User FROM mysql.user WHERE User like 'merlot';
```
 - If the MySQL user exists it will return
```
+-----------+--------+
| Host      | User   |
+-----------+--------+
| localhost | merlot |
+-----------+--------+
1 row in set (0.01 sec)
```
 - If the MySQL use does not exist, create it by running
```
CREATE USER 'merlot'@'localhost' IDENTIFIED BY 'merlot';
```

## Run app
From the parent directory run:
```
make run
```

## Run unit tests
Unit tests can be run locally or on a docker container
### Run unit tests locally
```
make build
make test
```
### Run unit tests on docker container
```
make build docker
make test docker
```
<!-- 
## Description
This is an internet browser running PyQt6 on the frontend and python 3.11 on the backend. MySQL is utilized for data storage, manipulation, and display. Open About dialog on app for more information.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser. -->

## Authors and acknowledgment
Author: Adam Glick
LinkedIn: https://www.linkedin.com/in/glicka

## License
GNU Lesser General Public License v3.0
<!-- 
## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers. -->
