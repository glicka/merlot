import pytest 

class TestMerlotDB:
    @pytest.fixture(autouse=True)
    def setup_method(self, mocker):
        with mocker.patch("mysql.connector.connect") as mockMySqlConnect, mocker.patch("mysql.connector.errors") as mockMySqlErrors:
            from merlot.sql import merlotDB
            self.db = merlotDB()

    def testDbName(self):
        assert self.db.name == "MerlotDB"

    def testResetIndex(self):
        self.db.db.reset_mock()
        self.db.resetIndex()
        assert self.db.cursor.execute.call_count == 6

    def testInsertRecord(self):
        qry =   """
                INSERT INTO History.Day (Time, Address)
                VALUES (NOW(), "https://duckduckgo.com")
                """
        self.db.insertRecord("https://duckduckgo.com")

        self.db.cursor.execute.assert_called_with(qry)

    def testAddBookmark(self):
        testBookmark = {
            "name": "DuckDuckGo",
            "address": "https://www.duckduckgo.com"
        }
        qry =   """
                INSERT INTO History.Bookmarks (Name, Address)
                VALUES ("{name}", "{address}")
                ON DUPLICATE KEY UPDATE History.Bookmarks.ID=History.Bookmarks.ID
                """.format(**testBookmark)
        self.db.addBookmark(testBookmark)

        self.db.cursor.execute.assert_called_with(qry)

    def testAddBookmarkValueError(self):
        testBookmark = {"name": "", "address": ""}
        with pytest.raises(ValueError) as err:
            self.db.addBookmark(testBookmark)
            assert str(err) == "Inputs cannot be blank!"

    def testRemoveBookmark(self):
        testBookmark = {
            "name": "DuckDuckGo",
            "address": "https://www.duckduckgo.com"
        }
        qry =   """
                DELETE FROM History.Bookmarks
                WHERE History.Bookmarks.Name="{name}"
                """.format(**testBookmark)

        self.db.removeBookmark(testBookmark)

        self.db.cursor.execute.assert_called_with(qry)

    def testRemoveBookmarkValueError(self):
        testBookmark = {"name": "", "address": ""}
        with pytest.raises(ValueError) as err:
            self.db.removeBookmark(testBookmark)
            assert str(err) == "Inputs cannot be blank!"
