import pytest
from PySide6.QtWidgets import QApplication
from sys import argv
import sys
from PySide6.QtCore import QUrl

class TestMerlotBrowser:
    @pytest.fixture(autouse=True)
    def setup_method(self, mocker):
        if not QApplication.instance():
            testApp = QApplication(argv)
        else:
            testApp = QApplication.instance()

        with mocker.patch("backend.merlotLogger") as mockLogger, mocker.patch("sql.merlotDB") as mockDB, mocker.patch("backend.persistantProcs") as mockProcs, mocker.patch("attributes.menu") as mockMenu, mocker.patch("PySide6.QtWidgets.QMainWindow.setMenuBar") as mockSetMenuBar:
            from merlot.merlot import Browser
            self.brows = Browser()

    def testBrowser(self):
        assert self.brows.procs.runProcs.call_count == 1
        
    def testGoToURL(self):
        testURL = QUrl("www.duckduckgo.com")
        self.brows.goToURL(testURL)
        self.brows.db.insertRecord.assert_called_with("https:www.duckduckgo.com")