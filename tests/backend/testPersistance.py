import pytest 

class TestPersistance:
    @pytest.fixture(autouse=True)
    def setup_method(self, mocker):
        with mocker.patch("backend.merlotLogger") as mockLogger, mocker.patch("multiprocessing.Process") as mockProcess:
            from merlot.backend import persistantProcs
            self.procs = persistantProcs(db = mocker.MagicMock())

    def testRollupHistoryTables(self):
        self.procs.db.reset_mock()
        self.procs.rollupHistoryTables()
        assert self.procs.db.insertWeeklyRecord.call_count == 1
        assert self.procs.db.insertAllRecord.call_count == 1

    def testRunProcs(self):
        self.procs.runProcs()
        assert self.procs.populateTables.start.call_count == 1

    def testKillProcs(self):
        self.procs.killProcs()
        assert self.procs.populateTables.terminate.call_count == 1