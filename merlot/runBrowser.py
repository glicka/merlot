from merlot import Browser
from PySide6 import QtCore
from PySide6.QtWidgets import QApplication
from sys import argv

QtCore.QCoreApplication.setAttribute(QtCore.Qt.AA_ShareOpenGLContexts)
app = QApplication(argv)
app.setApplicationName('Merlot Web Browser')
window = Browser()
window.show()
app.exec()