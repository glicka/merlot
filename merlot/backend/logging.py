import logging

class merlotLogger:
    def __init__(self, name=None):
        # levels = (logging.DEBUG, logging.INFO, logging.WARNING, logging.ERROR, logging.CRITICAL)
        logging.basicConfig(
            level = logging.DEBUG,
            format = '%(asctime)s %(name)-50s %(levelname)-8s %(message)-8s',
            datefmt = '%m-%d %H:%M',
            filename = '/tmp/merlot.log',
            filemode = 'a'
            )
        self.logger = logging.getLogger(name)
        self.name = name

    def formatString(self, msg, *args, **kwargs):
        """
        Capitalizes the first letter of a string
        """
        return msg[0].upper() + msg[1:]

    def debug(self, msg=''):
        """
        Wrapper for logger.debug method. Formats the debug message to ensure it writes
        to the log file.
        """
        if not msg:
            raise(ValueError("DEBUG message cannot be blank!"))

        if not msg[0].isupper():
            msg = self.formatString(msg)

        self.logger.debug(msg)

    def info(self, msg=''):
        """
        Wrapper for logger.info method. Formats the debug message to ensure it writes
        to the log file.
        """
        if not msg:
            raise(ValueError("INFO message cannot be blank!"))

        if not msg[0].isupper():
            msg = self.formatString(msg)

        self.logger.info(msg)

    def warning(self, msg=''):
        """
        Wrapper for logger.warning method. Formats the debug message to ensure it writes
        to the log file.
        """
        if not msg:
            raise(ValueError("WARNING message cannot be blank!"))

        if not msg[0].isupper():
            msg = self.formatString(msg)

        self.logger.warning(msg)

    def error(self, msg=''):
        """
        Wrapper for logger.error method. Formats the debug message to ensure it writes
        to the log file.
        """
        if not msg:
            raise(ValueError("ERROR message cannot be blank!"))
        if not msg[0].isupper():
            msg = self.formatString(msg)
        self.logger.error(msg)

    def critical(self, msg=''):
        """
        Wrapper for logger.critical method. Formats the debug message to ensure it writes
        to the log file.
        """
        if not msg:
            raise(ValueError("CRITICAL message cannot be blank!"))
        
        if not msg[0].isupper():
            msg = self.formatString(msg)

        self.logger.critical(msg)

    
        