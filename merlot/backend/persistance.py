from datetime import datetime as dt
import time 
from backend import merlotLogger
from multiprocessing import Process
import atexit

class persistantProcs:
    def __init__(self, db=None, logger=None):
        self.db = db 
        if logger is not None:
            loggerName = "{}.{}.{}".format(logger.name, __name__, self.__class__.__name__)
        else:
            loggerName = "{}.{}.{}".format("merlot", __name__, self.__class__.__name__)
        self.logger = merlotLogger(loggerName)
        self.now = dt.now()
        self.lastRollup = dt(2000, 1, 1) # Forces history rollup to run upon opening browser

        # Define process
        self.populateTables = Process(target = self.persistHistory())

        # Kill persistant processes when closing browser
        atexit.register(self.killProcs)

    def rollupHistoryTables(self):
        self.db.insertWeeklyRecord()
        self.db.insertAllRecord()
        self.lastRollup = dt.now()

    def clearHistoryTables(self):
        self.db.clearDailyHistory()
        self.db.clearWeeklyHistory()

    def persistHistory(self):
        self.now = dt.now()
        if self.now.__sub__(self.lastRollup).total_seconds() >= 15*60:
            self.logger.info("time to roll up history!")
            self.rollupHistoryTables()
            self.logger.info("time to clear history tables!")
            self.clearHistoryTables()

    def runProcs(self):
        # Create persistant processes to populate the history tables
        self.populateTables.start()
        

    def killProcs(self):
        # Kill processes
        self.populateTables.terminate()
            
