from mysql.connector import connect, errors
from backend import merlotLogger
import pandas as pd
import warnings




class merlotDB:
    def __init__(self, logger=None):
        """
        Class to connect to MySQL and execute commands directly involed 
        with the History database
        """
        self.db = connect(
            host="localhost",
            user="merlot",
            password="merlot",
            autocommit=True
        )

        self.cursor = self.db.cursor(buffered=True)

        # Instantiate class logger
        if logger is not None:
            loggerName = "{}.{}.{}".format(logger.name, __name__, self.__class__.__name__)
        else:
            loggerName = "{}.{}.{}".format("private", __name__, self.__class__.__name__)
        self.logger = merlotLogger(loggerName)

        self.createHistory()
        self.createHistoryTables()
        self.createBookmarkTable()
        self.resetIndex()

        # For testing purposes
        self.name = "MerlotDB"

    def resetIndex(self):
        # Reset the database index
        try:
            params = [{"table": "Day"}, {"table": "Week"}, {"table": "All"}]
            increments = []
            qry = """SELECT ID FROM History.{table} ORDER BY Time DESC LIMIT 1"""
            for param in params:
                self.cursor.execute(qry.format(**param))
                ret = self.cursor.fetchall()
                if ret:
                    ret = ret[0][0]
                    increments.append({"table": param["table"], "increment": ret})
                else:
                    increments.append({"table": param["table"], "increment": 1})

            qry = """ALTER TABLE History.{table} AUTO_INCREMENT = {increment}"""
            for increment in increments:
                self.cursor.execute(qry.format(**increment))

        except errors.DatabaseError as e:
            self.logger.error("error logged: {}".format(str(e)))

    def createHistory(self):
        """
        Create a history database
        """
        try:

            qry = """CREATE DATABASE IF NOT EXISTS History;"""

            ret = self.cursor.execute(qry)
            
        except errors.DatabaseError as e:
            self.logger.error("error logged: {}".format(str(e)))

    def createHistoryTables(self):
        """
        Create Day, Week, and All tables to contain history information
        """
        try:
            params = [{"table": "Day"}, {"table": "Week"}, {"table": "All"}]
            qry =   """
                    CREATE TABLE IF NOT EXISTS History.{table} (
                        ID INT AUTO_INCREMENT PRIMARY KEY,
                        Time DATETIME,
                        Address VARCHAR(255)                   
                    )
                    """
            for param in params:
                self.cursor.execute(qry.format(**param))
        except errors.DatabaseError as e:
            self.logger.error("error logged: {}".format(str(e)))


    def createBookmarkTable(self):
        """
        Create a Bookmark table and auto-populate it if it doesn't already
        exist
        """
        try:
            
            qry =   """
                    CREATE TABLE History.Bookmarks (
                        ID INT AUTO_INCREMENT PRIMARY KEY,
                        Name VARCHAR(255),
                        Address VARCHAR(255)                   
                    )
                    """
    
            self.cursor.execute(qry)

            params = [
                ("DailyWire", "https://www.dailywire.com/"),
                ("Rumble", "https://rumble.com/"),
                ("Bongino Report", "https://bonginoreport.com/"),
                ("LinkedIn", "https://www.linkedin.com/")
            ]
            qry =   """
                    INSERT INTO History.Bookmarks (Name, Address)
                    VALUES (%s, %s)
                    """
            self.cursor.executemany(qry, params)

        except errors.DatabaseError as e:
            self.logger.error("error logged: {}".format(str(e)))

    def insertRecord(self, record):
        qry =   """
                INSERT INTO History.Day (Time, Address)
                VALUES (NOW(), "{record}")
                """
        params = {"record": record}
        self.cursor.execute(qry.format(**params))

    def getDailyRecords(self):
        warnings.filterwarnings('ignore')
        qry =   """
                SELECT Time, Address
                FROM History.Day
                ORDER BY Time Desc
                """
        ret = pd.read_sql(qry, self.db)
        if ret.size != 0:
            return ret

    def getWeeklyRecords(self):
        warnings.filterwarnings('ignore')
        qry =   """
                SELECT Time, Address
                FROM History.Week
                ORDER BY Time Desc
                """
        ret = pd.read_sql(qry, self.db)
        if ret.size != 0:
            return ret

    def getAllRecords(self):
        warnings.filterwarnings('ignore')
        qry =   """
                SELECT Time, Address
                FROM History.All
                ORDER BY Time Desc
                """
        ret = pd.read_sql(qry, self.db)
        if ret.size != 0:
            return ret

    def getBookmarks(self):
        warnings.filterwarnings('ignore')
        qry =   """
                SELECT Name, Address
                From History.Bookmarks
                ORDER BY ID
                """
        ret = pd.read_sql(qry, self.db)
        if ret.size != 0:
            return ret

    def addBookmark(self, params: dict):
        if not params["name"] or not params["address"]:
            raise ValueError("Inputs cannot be blank!")
        qry =   """
                INSERT INTO History.Bookmarks (Name, Address)
                VALUES ("{name}", "{address}")
                ON DUPLICATE KEY UPDATE History.Bookmarks.ID=History.Bookmarks.ID
                """
        self.cursor.execute(qry.format(**params))

    def removeBookmark(self, params: dict):
        if not params["name"]:
            raise ValueError("Inputs cannot be blank!")
        qry =   """
                DELETE FROM History.Bookmarks
                WHERE History.Bookmarks.Name="{name}"
                """
        self.cursor.execute(qry.format(**params))


    def insertWeeklyRecord(self):
        qry =   """
                INSERT INTO History.Week (Time, Address)
                    SELECT Time, Address 
                    FROM History.Day 
                    WHERE Time > DATE_SUB(NOW(), INTERVAL 7 DAY)
                ON DUPLICATE KEY UPDATE History.Week.ID=History.Week.ID
                """
        self.cursor.execute(qry)

    def insertAllRecord(self):
        qry =   """
                INSERT INTO History.All (Time, Address)
                    SELECT Time, Address 
                    FROM History.Day
                ON DUPLICATE KEY UPDATE History.All.ID=History.All.ID
                """
        self.cursor.execute(qry)

    def clearDailyHistory(self):
        # Remove items from History.Day that are older than 24 hours
        qry =   """
                DELETE FROM History.Day
                WHERE Time < DATE_SUB(NOW(), INTERVAL 24 HOUR)
                """
        self.cursor.execute(qry)

    def clearWeeklyHistory(self):
        # Remove items from History.Week that are older than 7 days
        qry =   """
                DELETE FROM History.Week
                WHERE Time < DATE_SUB(NOW(), INTERVAL 7 DAY)
                """
        self.cursor.execute(qry)

    def clearHistory(self):
        # Delete all entries in Day, Week, and All tables
        qry =   """
                DELETE FROM History.{}
                """
        tables = ["Day", "Week", "All"]

        [self.cursor.execute(qry.format(i)) for i in tables]