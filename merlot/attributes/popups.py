from PySide6.QtWidgets import *
from PySide6.QtCore import QAbstractTableModel, Qt
from PySide6.QtGui import QAction
from backend import merlotLogger

class aboutWindow(QMessageBox):
    """
    This "window" is a QWidget. If it has no parent, it
    will appear as a free-floating window as we want.
    """
    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.setWindowTitle("About")
        self.setWindowModality(Qt.ApplicationModal)

    
        self.setText(
            "{:^80}\n\n{:^80}\n{:^80}\n{:^80}\n{:^80}".format(
                self.aboutMsg(), 
                self.website(), 
                self.author(), 
                self.contact(), 
                self.license()
                )
            )

    def aboutMsg(self):
        return "Merlot Web Browser v0.0.1"
    
    def author(self):
        return "author: Adam Glick"

    def contact(self):
        return "contact: glicka@proton.me"

    def license(self):
        return "GNU Lesser General Public License v3.0"

    def website(self):
        return "https://gitlab.com/glicka/merlot"


class addBookmark(QWidget):
    def __init__(self, parent=None, db=None, logger=None):
        super().__init__(parent=parent)
        self.db = db
        if logger is not None:
            # Create a logger
            loggerName = "{}.{}.{}".format(logger.name, __name__, self.__class__.__name__)
            self.logger = merlotLogger(loggerName)
        form = QFormLayout()

        self.siteNickname = QLineEdit(self)
        self.siteNickname.setPlaceholderText("ex: DuckDuckGo")
        form.addRow("Site nickname: ", self.siteNickname)

        self.siteAddress = QLineEdit(self)
        self.siteAddress.setPlaceholderText("https://...")
        form.addRow("Site address: ", self.siteAddress)

        self.button = QPushButton('Save')
        self.button.clicked.connect(self.handleButton)
        form.addRow(self.button)
        self.setLayout(form)

        self.setWindowTitle('Add bookmark')
        self.hide()

    def handleButton(self):
        params = {"name": self.siteNickname.text(), "address": self.siteAddress.text()}
        try:
            self.db.addBookmark(params)
        except Exception as e:
            if hasattr(self, "logger"):
                self.logger.error("Exception caught: {}".format(str(e)))
            else:
                pass
        self.close()

class deleteBookmark(QWidget):
    def __init__(self, parent=None, db=None, logger=None):
        super().__init__(parent=parent)
        self.resize(250, 100)
        self.db = db
        if logger is not None:
            # Create a logger
            loggerName = "{}.{}.{}".format(logger.name, __name__, self.__class__.__name__)
            self.logger = merlotLogger(loggerName)
        form = QFormLayout()

        self.dropdown = QComboBox(self)

        # Populate combo box with current bookmarks
        self.dropdown.addItem("Bookmarks")
        for item in self.populateMenu():
            self.dropdown.addItem(item)

        form.addRow(self.dropdown)

        # Add combo box to widget
        self.setLayout(form)

        # Setup button to execute action when pushed
        self.button = QPushButton('Remove')
        self.button.clicked.connect(self.handleButton)
        form.addRow(self.button)
        self.setLayout(form)

        self.setWindowTitle('Remove bookmark')
        self.hide()

    def populateMenu(self): 
        self.bookmarkData = self.db.getBookmarks()
        for i in range(self.bookmarkData.shape[0]):
            bookmark = self.bookmarkData.Name[i]
            yield bookmark

    def handleButton(self):
        params = {"name": self.dropdown.currentText()}
        try:
            self.db.removeBookmark(params)
        except Exception as e:
            if hasattr(self, "logger"):
                self.logger.error("Exception caught: {}".format(str(e)))
            else:
                pass
        self.close()

class tableModel(QAbstractTableModel):
    def __init__(self, data):
        super().__init__()
        self._data = data

    def data(self, index, role):
        if role == Qt.DisplayRole:
            value = self._data.iloc[index.row(), index.column()]
            return str(value)

    def rowCount(self, index):
        return self._data.shape[0]

    def columnCount(self, index):
        return self._data.shape[1]

    def headerData(self, section, orientation, role):
        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                return(str(self._data.columns[section]))