from PySide6.QtWidgets import QMenuBar, QTableView, QHeaderView
from PySide6.QtGui import QAction, QIcon
from PySide6.QtCore import QUrl, QAbstractTableModel, Qt

import pandas as pd

from .popups import aboutWindow, tableModel, addBookmark, deleteBookmark
from .private import privateBrowser
from backend import merlotLogger

class menu:
    def __init__(self, db, parent, logger=None):

        self.db = db
        # Adding a menu for history, bookmarks, etc
        self.menu = QMenuBar(parent)
        self.parent = parent
        if logger is not None:
            # Create a logger
            loggerName = "{}.{}.{}".format(logger.name, __name__, self.__class__.__name__)
            self.logger = merlotLogger(loggerName)

        customizable = self.menu.addMenu(QIcon("icons/menu.png"),"")
        # Add history menu
        history = customizable.addMenu("History")
        dailyHistory = QAction("Daily", history)
        dailyHistory.triggered.connect(lambda: self.displayHistory("day"))
        weeklyHistory = QAction("Weekly", history)
        weeklyHistory.triggered.connect(lambda: self.displayHistory("week"))
        allHistory = QAction("All", history)
        allHistory.triggered.connect(lambda: self.displayHistory("all"))
        clearHistory = QAction("Clear", history)
        clearHistory.triggered.connect(lambda: self.db.clearHistory())
        # Add about menu
        about = customizable.addAction("About")
        about.triggered.connect(lambda: self.displayAbout())
        # Private browser
        private = customizable.addAction("Private window")
        private.triggered.connect(lambda: self.openPB())
        
        history.addAction(dailyHistory)
        history.addAction(weeklyHistory)
        history.addAction(allHistory)
        history.addAction(clearHistory)

        # Create bookmarks menu
        self.bookmarks = self.menu.addMenu("Bookmarks")
        # Define bookmarks to dynamically update
        self.bookmarks.aboutToShow.connect(self.updateBookmarks)
        

    def updateBookmarks(self):
        """
        Method to populate bookmarks whenever a change is made
        """
        # Clear bookmarks menu
        self.bookmarks.clear()
        # Populate bookmarks menu
        for action in self.createBookmarks():
            self.bookmarks.addAction(action)
        # Create submenu to edit bookmarks menu
        editBookmarks = self.bookmarks.addMenu("{:^30}".format("+"))
        # Action to add a bookmark
        addBookmark = QAction("{:^30}".format("add"), editBookmarks)
        addBookmark.triggered.connect(lambda: self.addBookmark())
        # Add action to submenu
        editBookmarks.addAction(addBookmark)
        # Action to delete a bookmark
        removeBookmark = QAction("{:^30}".format("remove"), editBookmarks)
        removeBookmark.triggered.connect(lambda: self.removeBookmark())
        # Add action to submenu
        editBookmarks.addAction(removeBookmark)

    def createBookmarks(self): 
        """
        Method to create bookmarks actions based on what is in database
        """
        self.bookmarkData = self.db.getBookmarks()
        for i in range(self.bookmarkData.shape[0]):
            action = QAction(self.bookmarkData.Name[i], self.parent)
            action.setStatusTip("Go to {}".format(self.bookmarkData.Name[i]))
            bookmarkURL = QUrl("{}".format(self.bookmarkData.Address[i]))
            action.triggered.connect(lambda checked=True, url=bookmarkURL: self.parent.goToURL(url))
            yield action

    def addBookmark(self):
        self.abmpu = addBookmark(db=self.db, logger=self.parent.logger)
        self.abmpu.show()

    def removeBookmark(self):
        self.rbmpu = deleteBookmark(db=self.db, logger=self.parent.logger)
        self.rbmpu.show()
        self.parent.menu = self.menu

    def displayHistory(self, record):
        match record.upper():
            case "DAY":
                # Grab data from History.Day
                self.dailyHistory = self.db.getDailyRecords()
                try:
                    # Create a history tab
                    self.table = QTableView()
                    self.table.setModel(tableModel(self.dailyHistory))
                    self.table.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
                    i = self.parent.windowTabs.addTab(self.table, "Daily History")
                    self.parent.windowTabs.setCurrentIndex(i)
                except AttributeError as e:
                    self.logger.error("Daily history has no data!")
                    self.logger.error("AttributeError: {}".format(str(e)))

            case "WEEK":
                # Grab data from History.Week
                self.weeklyHistory = self.db.getWeeklyRecords()
                try:
                    # Create a history tab
                    self.table = QTableView()
                    self.table.setModel(tableModel(self.weeklyHistory))
                    self.table.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
                    i = self.parent.windowTabs.addTab(self.table, "Weekly History")
                    self.parent.windowTabs.setCurrentIndex(i)
                except AttributeError as e:
                    self.logger.error("Weekly history has no data!")
                    self.logger.error("AttributeError: {}".format(str(e)))

            case "ALL":
                # Grab data from History.Week
                self.weeklyHistory = self.db.getAllRecords()
                try:
                    # Create a history tab
                    self.table = QTableView()
                    self.table.setModel(tableModel(self.weeklyHistory))
                    self.table.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
                    i = self.parent.windowTabs.addTab(self.table, "All History")
                    self.parent.windowTabs.setCurrentIndex(i)
                except AttributeError as e:
                    self.logger.error("All history has no data!")
                    self.logger.error("AttributeError: {}".format(str(e)))

    def displayAbout(self):
        print("Going to display about window now")
        w = aboutWindow()
        w.exec_()

    def openPB(self):
        w = privateBrowser()