from sys import argv
from PySide6 import QtCore, QtWidgets, QtGui
from PySide6.QtGui import QAction, QIcon
from PySide6.QtCore import QUrl
from PySide6.QtWebEngineWidgets import QWebEngineView
#                                 Web Browser (HTML Frame)
from PySide6.QtWidgets import *

from sql import merlotDB
from backend import merlotLogger, persistantProcs
from .popups import aboutWindow, tableModel

class privateMenu:
    def __init__(self, db, parent, logger=None):

        self.db = merlotDB(logger = logger)
        # Adding a menu for history, bookmarks, etc
        self.menu = QMenuBar(parent)
        self.parent = parent
        if logger is not None:
            # Create a logger
            loggerName = "{}.{}.{}".format(logger.name, __name__, self.__class__.__name__)
            self.logger = merlotLogger(loggerName)

        customizable = self.menu.addMenu(QIcon("icons/menu.png"),"")
        # Add about menu
        about = customizable.addAction("About")
        about.triggered.connect(lambda: self.displayAbout())
        # Private browser
        private = customizable.addAction("Private window")
        private.triggered.connect(lambda: self.openPB())

        bookmarks = self.menu.addMenu("Bookmarks")
        for action in self.createBookmarks():
            bookmarks.addAction(action)

    def createBookmarks(self): 
        self.bookmarkData = self.db.getBookmarks()
        for i in range(self.bookmarkData.shape[0]):
            action = QAction(self.bookmarkData.Name[i], self.parent)
            action.setStatusTip("Go to {}".format(self.bookmarkData.Name[i]))
            bookmarkURL = QUrl("{}".format(self.bookmarkData.Address[i]))
            action.triggered.connect(lambda checked=True, url=bookmarkURL: self.parent.goToURL(url))
            yield action

    def displayAbout(self):
        print("Going to display about window now")
        w = aboutWindow()
        w.exec_()

    def openPB(self):
        w = privateBrowser()

class privateBrowser(QMainWindow):
   def __init__(self, *args, **kwargs):
      super().__init__(*args, **kwargs)

      # Set window title
      self.setWindowTitle("(Private) Merlot Browser")

      # Create a tab widget
      self.windowTabs = QTabWidget()

      # making document mode true
      self.windowTabs.setDocumentMode(True)

      # adding action when double clicked
      self.windowTabs.tabBarDoubleClicked.connect(self.tabOpenDoubleclick)

      # adding action when tab is changed
      self.windowTabs.currentChanged.connect(self.currentTabChanged)

      # making tabs closeable
      self.windowTabs.setTabsClosable(True)

      # adding action when tab close is requested
      self.windowTabs.tabCloseRequested.connect(self.closeCurrentTab)

      # making tabs as central widget
      self.setCentralWidget(self.windowTabs)

      # creating first tab
      self.addNewTab(QUrl('http://www.duckduckgo.com'), 'Homepage')
      
      self.statusBar = QStatusBar()
      self.setStatusBar(self.statusBar)
      self.navigationBar = QToolBar('Navigation Toolbar')
      self.addToolBar(self.navigationBar)
      backButton = QAction("Back", self)
      backButton.setStatusTip('Go to previous page you visited')
      backButton.triggered.connect(self.windowTabs.currentWidget().back())
      self.navigationBar.addAction(backButton)
      refreshButton = QAction("Refresh", self)
      refreshButton.setStatusTip('Refresh this page')
      refreshButton.triggered.connect(self.windowTabs.currentWidget().reload())
      self.navigationBar.addAction(refreshButton)
      nextButton = QAction("Next", self)
      nextButton.setStatusTip('Go to next page')
      nextButton.triggered.connect(self.windowTabs.currentWidget().forward())
      self.navigationBar.addAction(nextButton)
      homeButton = QAction("Home", self)
      homeButton.setStatusTip('Go to home page (DuckDuckGo)')
      homeButton.triggered.connect(self.goToHome)
      self.navigationBar.addAction(homeButton)
      self.navigationBar.addSeparator()
      self.URLBar = QLineEdit()
      self.URLBar.returnPressed.connect(lambda: self.goToURL(QUrl(self.URLBar.text())))  # This specifies what to do when enter is pressed in the Entry field
      self.navigationBar.addWidget(self.URLBar)
      self.addToolBarBreak()

      # Adding a menu for history, bookmarks, etc
      self.setMenuBar(privateMenu(db = None, parent = self, logger = None).menu)

      # Set the main window icon that represents the app
      self.setWindowIcon(QtGui.QIcon("icons/husky2.jpg"))

      # Show the browser
      self.show()

   def goToHome(self):
      self.windowTabs.currentWidget().setUrl(QUrl('https://www.duckduckgo.com/'))

   def goToURL(self, url: QUrl):
      if url.scheme() == '':
         url.setScheme('https')
      self.windowTabs.currentWidget().setUrl(url)
      self.updateAddressBar(url)

   def updateAddressBar(self, url):
      self.URLBar.setText(url.toString())
      self.URLBar.setCursorPosition(0)

   # method for adding new tab
   def addNewTab(self, qurl = None, label ="Blank"):

      # if url is blank
      if qurl is None:
         # creating a google url
         qurl = QUrl('http://www.duckduckgo.com')

      # creating a QWebEngineView object
      browser = QWebEngineView()

      # setting url to browser
      browser.setUrl(qurl)

      # setting tab index
      i = self.windowTabs.addTab(browser, label)
      self.windowTabs.setCurrentIndex(i)

      # adding action to the browser when url is changed
      # update the url
      browser.urlChanged.connect(lambda qurl, browser = browser:
                                 self.updateURLBar(qurl, browser))

      # adding action to the browser when loading is finished
      # set the tab title
      browser.loadFinished.connect(lambda _, i = i, browser = browser:
                                    self.windowTabs.setTabText(i, browser.page().title()))

   # when double clicked is pressed on tabs
   def tabOpenDoubleclick(self, i):

      # checking index i.e
      # No tab under the click
      if i == -1:
         # creating a new tab
         self.addNewTab()

   # when tab is changed
   def currentTabChanged(self, i):
      if type(self.windowTabs.currentWidget()) == "PySide6.QtWebEngineWidgets.QWebEngineView":
         # get the curl
         qurl = self.windowTabs.currentWidget().url()

         # update the url
         self.updateURLBar(qurl, self.windowTabs.currentWidget())

         # update the title
         self.updateTitle(self.windowTabs.currentWidget())

   # when tab is closed
   def closeCurrentTab(self, i):

      # if there is only one tab
      if self.windowTabs.count() < 2:
         # do nothing
         return

      # else remove the tab
      self.windowTabs.removeTab(i)

   # method for updating the title
   def updateTitle(self, browser):

      # if signal is not from the current tab
      if browser != self.windowTabs.currentWidget():
         # do nothing
         return

      # get the page title
      title = self.windowTabs.currentWidget().page().title()

      # set the window title
      self.setWindowTitle(title)

   # method to update the url
   def updateURLBar(self, q, browser = None):

      # If this signal is not from the current tab, ignore
      if browser != self.windowTabs.currentWidget():

         return

      # set text to the url bar
      self.URLBar.setText(q.toString())

      # set cursor position
      self.URLBar.setCursorPosition(0)
